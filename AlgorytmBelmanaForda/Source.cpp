#include <iostream>
#include <vector>
using namespace std;

const int maxLiczbaWierzcholkow = 101; //lepiej dac 1 wiecej dla pewnosci, STL i tak stworzy 124
short int nieskonczonosc = 32767;

class Graf {
private:
	vector<vector<short int> > krawedzie;
public:
	int liczbaWierzcholkow;
	Graf();
	//~Graf();
	void zerowanie();
	void ustawKrawedz(int x, int y, short int wartosc);
	short int zwrocKrawedz(int x, int y);
};
Graf::Graf() {
	this->liczbaWierzcholkow = maxLiczbaWierzcholkow;
	for (int i = 0; i < maxLiczbaWierzcholkow; ++i)
	{
		krawedzie.resize(i, vector<short int>(maxLiczbaWierzcholkow, 0));
	}
}
//Graf::~Graf() {
//	//zbiorWierzcholkow.clear();
//	for (int i = 0; i < maxLiczbaWierzcholkow - 1; ++i)
//	{
//		krawedzie[i].clear();
//	}
//	krawedzie.clear();
//}
void Graf::zerowanie()
{
	for (int i = 0; i < liczbaWierzcholkow; ++i)
	{
		for (int j = 0; j < liczbaWierzcholkow; ++j)
		{
			krawedzie[i][j] = 0;
		}
	}
	liczbaWierzcholkow = 0;
}
void Graf::ustawKrawedz(int x, int y, short int wartosc)
{
	krawedzie[x][y] = wartosc;
}
short int Graf::zwrocKrawedz(int x, int y)
{
	return krawedzie[x][y];
}
void wypisz(short int *tablica, int rozmiar)
{
	for (int i = 0; i < rozmiar; ++i)
		if (tablica[i] >= nieskonczonosc - 100)
			cout << "0 "; //cout << "* ";
		else
			cout << tablica[i] << " ";
	cout << "\n";
}
void BellmanFord(Graf *G, short int *tablicaOdleglosci)
{
	//--------SET TABLICY ODLEGLOSCI---------
	tablicaOdleglosci[0] = 0;
	for (int i = 1; i < G->liczbaWierzcholkow; ++i) {
		tablicaOdleglosci[i] = nieskonczonosc;
	}

	//-------------RESZTA--------------------
	for (int k = 0; k < G->liczbaWierzcholkow - 1; ++k)
	{
		for (int x = 0; x < G->liczbaWierzcholkow; ++x) //aktualny
		{
			for (int y = 0; y < G->liczbaWierzcholkow; ++y) //docelowy ( foreach )
			{
				if (G->zwrocKrawedz(x, y) != 0 && tablicaOdleglosci[x] + G->zwrocKrawedz(x, y) < tablicaOdleglosci[y])
				{
					tablicaOdleglosci[y] = tablicaOdleglosci[x] + G->zwrocKrawedz(x, y);
				}
			}
		}
		wypisz(tablicaOdleglosci, G->liczbaWierzcholkow);
	}
	cout << "\n";
}
int main()
{
	//--------------INIT-------------------
	int liczbaTestow, liczbaWierzcholkow;
	short int krawedz, tablicaOdleglosci[maxLiczbaWierzcholkow];
	Graf G; //tworzymy maxymalny zeby nie marnowac czasu na alokowanie pamieci

			//----------WCZYTYWANIE DANYCH----------------
	cin >> liczbaTestow;

	for (int k = 0; k < liczbaTestow; ++k)
	{
		cin >> liczbaWierzcholkow;
		G.liczbaWierzcholkow = liczbaWierzcholkow;
		for (int x = 0; x < liczbaWierzcholkow; ++x)
			for (int y = 0; y < liczbaWierzcholkow; ++y)
			{
				cin >> krawedz;
				G.ustawKrawedz(x, y, krawedz);
			}
		BellmanFord(&G, tablicaOdleglosci);
	}

	return 0;
}